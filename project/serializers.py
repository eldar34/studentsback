from rest_framework import serializers

from .models import Students

class StudentsListSerializer(serializers.ModelSerializer):
    """ Get Post List """

    class Meta:
        model = Students
        fields = "__all__"