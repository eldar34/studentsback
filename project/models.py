from django.db import models

# Create your models here.

class Students(models.Model):
    name = models.CharField(max_length=150)
    cource = models.CharField(max_length=150)
    raiting = models.IntegerField()

    class Meta:
        ordering = ['name']
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse("student", kwargs={"pk": self.pk})