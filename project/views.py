from django.shortcuts import render
from rest_framework import viewsets, renderers, permissions

# Create your views here.

from .models import Students
from .serializers import StudentsListSerializer

class PostReadViewSet(viewsets.ModelViewSet):
    queryset = Students.objects.all()
    permission_classes=[permissions.AllowAny]
    serializer_class = StudentsListSerializer
