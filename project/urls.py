from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'students', views.PostReadViewSet, basename='students')

urlpatterns = router.urls

# urlpatterns = [
#     path('students/', include('project.urls')),
# ]